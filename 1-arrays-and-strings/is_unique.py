# determine if a string has all unique characters

def is_unique_dict(word: str) -> bool:
    chars = {}
    for char in word:
        if char in chars.keys():
            return False
        chars[char] = 1
    return True

def is_unique_list(word: str) -> bool:
    chars = []
    for char in word:
        if char in chars:
            return False
        chars.append(char)
    return True

# what if you cannot use additional data structures
def is_unique_n2(word: str) -> bool:
    for i, char in enumerate(word):
        for j, other_char in enumerate(word):
            if i != j and char == other_char:
                return False
    return True

def is_unique_nlogn(word: str) -> bool:
    # check only the following letters if they are the same
    # because the previous ones were already checked
    # O(n*log n)
    for i in range(len(word)-1):
        j = i + 1
        while j < len(word):
            if word[i] == word[j]:
                return False
            j += 1
    return True
        

unique_word = "abcd"
not_unique_word = "abcda"

print("DICT")
print(f"{unique_word} is unique: {is_unique_dict(unique_word)}")
print(f"{not_unique_word} is unique: {is_unique_dict(not_unique_word)}")

print("LIST")
print(f"{unique_word} is unique: {is_unique_list(unique_word)}")
print(f"{not_unique_word} is unique: {is_unique_list(not_unique_word)}")

print("NO STRUCT n2")
print(f"{unique_word} is unique: {is_unique_n2(unique_word)}")
print(f"{not_unique_word} is unique: {is_unique_n2(not_unique_word)}")

print("NO STRUCT n*logn")
print(f"{unique_word} is unique: {is_unique_nlogn(unique_word)}")
print(f"{not_unique_word} is unique: {is_unique_nlogn(not_unique_word)}")
